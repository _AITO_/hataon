from django.urls import path
from .views import SignUpView
from django.views.generic.base import TemplateView


urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('users/password_reset/', TemplateView.as_view(template_name='password_reset_form.html'), name='password_reset'),
]