from django import forms

from .models import File


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = ('name_of_file', 'info', 'file', 'cover')
